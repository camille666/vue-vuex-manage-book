/**
 * getters
 * 共享方法
 * 获取从state派生的的bid状态
 */
export const getterFilterBid = state => {
	const bIdArr = [];
	state.book.forEach(function(ele,index){
		bIdArr.push(ele['bId']);
	});
	return bIdArr;
}