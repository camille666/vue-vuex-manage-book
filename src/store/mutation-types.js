/**
 * mutaion-types
 * 定义全局共享的mutation类型
 * 采用常量大写方式
 */
export const ADD_BOOK = 'ADD_BOOK';
export const DELETE_BOOK = 'DELETE_BOOK';
export const UPDATE_BOOK = 'UPDATE_BOOK';
export const UPDATE_BOOK_INDEX = 'UPDATE_BOOK_INDEX';
export const ALL_BOOK = 'ALL_BOOK';

