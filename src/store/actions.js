/**
 * actions
 * 同步或者异步，提交mutation
 */
import { ADD_BOOK, DELETE_BOOK, UPDATE_BOOK, UPDATE_BOOK_INDEX, ALL_BOOK } from './mutation-types';
import axios from 'axios';

function getData(contextCommit,mutationType,apiUrl){
	axios.get(apiUrl).then((response)=>{
		contextCommit(mutationType, response.data);
	}).catch((error)=>{
		//这里写一个无数据兼容
		console.log(error);
	})
}
export default {
	actionAddBook({commit}, bookObj){
		commit(ADD_BOOK, bookObj);
	},
	actionDeleteBook({commit}, index){
		commit(DELETE_BOOK, index);
	},
	actionUpdateBook({commit,state}, payload){
		commit(UPDATE_BOOK, payload);
	},
	actionUpdateBookIndex({commit,state}, payload){
		commit(UPDATE_BOOK_INDEX, payload);
	},
	actionAllBook({commit}, payload){
		getData(commit,ALL_BOOK,'../../ajax/book.json')
	}
}