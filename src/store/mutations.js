/**
 * mutations
 * 修改状态，增删改查
 */
import { ADD_BOOK, DELETE_BOOK, UPDATE_BOOK, UPDATE_BOOK_INDEX, ALL_BOOK } from './mutation-types';
import Vue from 'vue';

export default {
	[ADD_BOOK](state,bookObj){
		state.book.push(bookObj);
	}, 
	[DELETE_BOOK](state,index){
		state.book.splice(index,1);
	}, 
	[UPDATE_BOOK](state,payload){
		Vue.set(state.book,state.updateIndex,payload);
	},
	[UPDATE_BOOK_INDEX](state,payload){
		state.updateIndex = payload;
	},
	[ALL_BOOK](state,payload){
		state.book = payload;
	}
}