/*
 * 共享状态
 * book，用于存储所有图书信息
 * updateIndex，用于存储修改的图书位置
 */
export default {
	updateIndex: -1,
	book: []
}