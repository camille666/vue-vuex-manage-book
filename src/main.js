import Vue from 'vue'
import Pcom from './pcom.vue'
import store from './store/store.js'

var vm = new Vue({
  el: '#J_book_list',
  store,
  components: { 
  	'p-com': Pcom
  }
})