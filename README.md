# vueManageBook
vue，vuex实现图书管理后台。

## 我最近的读书列表

## 分支与实现方案

分支     | 独立文件 | 组件通信 | 动态读取数据 | 状态管理 | 难度系数
---------|--------|---------|------------|---------|---------
master   |    是   |    否   |      否     |  data  |  入门
noChat2   |    是   |    否   |   是(nginx) |  data  |  入门
simple1  |    是   | 简单通信 |      否     |  data  |  初级
simple2  |    是   | 简单通信 |   是(nginx) |  data  |  中级(常用)
vuex1    |    是   | vuex通信 |  是(nginx) |  state  |  高级(常用)
simple3  |单文件组件 | 简单通信 | 是(webpack)|  data   |  工程化(推荐)
vuex2    |单文件组件 | vuex通信| 是(webpack) |  state  |  高级工程化(常用)

* 分支命名和学习难度系数

他们俩取决于`组件是否通信`，是简单通信还是复杂通信？

* 独立文件和单文件组件

这里规定，独立文件是指，引入vue或者vuex库，直接按照vue语法写代码，适用于不会webpack的同学；
单文件组件是指，书写.vue文件，结合打包工具，引入vue和vuex，然后做项目，适用于掌握webpack的同学。

* 组件通信

如果把所有代码写在一个html文件和js文件中，可以不用组件通信，因为不涉及传递数据，数据直接在本地获取即可。这种情况适用于入门学习vue的同学，先了解语法。

如果想分模块管理文件，会涉及组件传递数据，势必要用到组件通信。这种情况适用于日常工作的同学，笔者推荐采用简单通信。

如果想做大型单页面应用，会涉及数据共享，可以采用vuex实现组件通信。

* 读取数据

如果只是为了尝试，可以写一些简单的数据，测试功能；

如果要做项目，通常很多数据来自后端，为了实现无缝对接，这里建议自己写ajax数据，然后动态读取，测试功能，可以放在nginx里面运行，也可以放在webpack里面运行。

## vuex2分支

* 渲染页面

````
异步读取书籍列表
````

* 添加图书信息

````
1、查询现有编号，防止添加的编号重复；

2、表单验证，给予提示；

3、过滤特殊字符。
````

* 删除图书信息

````
删除询问
````

* 更新图书信息

````
1、弹框，携带信息，父子组件传递信息；

2、编号栏不可以被修改，编号唯一性；

3、动态添加属性。
````

* 查询图书信息

````
1、筛选列表；

2、筛选后修改，筛选后删除；

3、筛选后没有值的显示。
````

## 数据相关

book是所有图书数据，filterBook用于存放筛选出的数据。

